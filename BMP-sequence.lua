-- Run the current pattern and save the selection as a separate .bmp file
-- for each generation.  The files are saved in the same folder as the script.
-- Author: Andrew Trevorrow (andrew@trevorrow.com), March 2017.

local g = golly()
local gp = require "gplus"
local chr = string.char
local getcell = g.getcell

if g.empty() then g.exit("There is no pattern.") end

local srect = g.getselrect()
if #srect == 0 then g.exit("There is no selection.") end
local x, y, wd, ht = table.unpack(srect)

-- avoid creating huge files
if wd * ht >= 100000000 then
   g.exit("Bitmap area is restricted to < 100 million cells.")
end

local colors = g.getcolors()     -- {0,r0,g0,b0, ... N,rN,gN,bN}
local state0 = chr(colors[4])..chr(colors[3])..chr(colors[2])
local statebgr = {}
for i = 1, g.numstates() - 1 do
    local j = i * 4 + 2
    statebgr[i] = chr(colors[j+2])..chr(colors[j+1])..chr(colors[j])
end

--------------------------------------------------------------------------------

-- Pack a given +ve integer into a little-endian binary string of size numbytes.
-- Based on code at http://lua-users.org/wiki/ReadWriteFormat.

local function lpack(numbytes, value)
    local result = ""
    for i = 1, numbytes do
        result = result .. chr(value % 256)
        value = math.floor(value / 256)
    end
    return result
end

--------------------------------------------------------------------------------

-- create a minimal dictionary with values for a Windows Version 3 DIB header:
local d = {
    mn1=66,
    mn2=77,
    filesize=0,
    undef1=0,
    undef2=0,
    offset=54,
    headerlength=40,
    width=wd,
    height=ht,
    colorplanes=1,
    colordepth=24,
    compression=0,
    imagesize=0,
    res_hor=0,
    res_vert=0,
    palette=0,
    importantcolors=0
}

-- we only need to create the header data once:
local header =
    lpack(1, d.mn1) ..
    lpack(1, d.mn2) ..
    lpack(4, d.filesize) ..
    lpack(2, d.undef1) ..
    lpack(2, d.undef2) ..
    lpack(4, d.offset) ..
    lpack(4, d.headerlength) ..
    lpack(4, d.width) ..
    lpack(4, d.height) ..
    lpack(2, d.colorplanes) ..
    lpack(2, d.colordepth) ..
    lpack(4, d.compression) ..
    lpack(4, d.imagesize) ..
    lpack(4, d.res_hor) ..
    lpack(4, d.res_vert) ..
    lpack(4, d.palette) ..
    lpack(4, d.importantcolors)

-- we may need to pad each row:
local row_mod = (d.width * d.colordepth / 8) % 4
local padding = ""
if row_mod > 0 then
    for i = 1, 4 - row_mod do
        padding = padding..chr(0)
    end
end

--------------------------------------------------------------------------------

function CreateBMPFile(filename)
    -- create a BMP file showing the pattern in the current selection
    local outfile = io.open(filename,'wb')
    if not outfile then g.exit("Unable to create BMP file:\n"..filename) end

    -- write the header info
    outfile:write(header)

    -- write the byte data (BMPs are encoded left-to-right from the bottom-up)
    for row = ht, 1, -1 do
        for col = 1, wd do
            local state = getcell(x + col - 1, row + y - 1)
            if state == 0 then
                outfile:write(state0)
            else
                outfile:write(statebgr[state])
            end
        end
        if row_mod > 0 then
            -- add padding so the byte count for each row is divisible by 4
            outfile:write(padding)
        end
    end

    outfile:close()
end

--------------------------------------------------------------------------------

-- remove any existing extension from layer name
local outname = gp.split(g.getname(),"%.")

-- prompt user for number of generations (= number of files)
local numgens = g.getstring("The number of generations will be\n" ..
                            "the number of .bmp files created.",
                            "100", "How many generations?")

-- prompt user for export path
local path =  g.getstring("Export folder path\n" ..
                            "(must end with slash / or antislash \\)",
                            "", "Export path")

if #numgens == 0 then
    g.exit()
end
if not gp.validint(numgens) then
    g.exit('Sorry, but "' .. numgens .. '" is not a valid integer.')
end

local numcreated = 0
local intgens = tonumber(numgens)
while intgens > 0 do
    local outfile = path .. outname .. "_" .. g.getgen() .. ".bmp"
    g.show("Creating " .. outfile)
    CreateBMPFile(outfile)
    numcreated = numcreated + 1
    if intgens == 1 then break end
    g.run(1)
    g.update()
    intgens = intgens - 1
end
g.show("Number of .bmp files created: " .. numcreated)
