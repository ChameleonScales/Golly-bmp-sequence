# This script was made by Andrew Trevorrow. Link to original post here http://conwaylife.com/forums/viewtopic.php?f=9&t=1342

# Run the current pattern and save the selection as a separate .bmp file
# for each generation.  The files are saved in the same folder as the script.
# Author: Andrew Trevorrow (andrew@trevorrow.com), March 2014.

import golly as g
from glife import validint
from glife.WriteBMP import WriteBMP
import os.path

if g.empty(): g.exit("There is no pattern.")

srect = g.getselrect()
if len(srect) == 0: g.exit("There is no selection.")
x = srect[0]
y = srect[1]
wd = srect[2]
ht = srect[3]

# prevent Python allocating a huge amount of memory
if wd * ht >= 100000000:
   g.exit("Bitmap area is restricted to < 100 million cells.")

multistate = g.numstates() > 2
colors = g.getcolors()     # [0,r0,g0,b0, ... N,rN,gN,bN]
state0 = (colors[1],colors[2],colors[3])

# --------------------------------------------------------------------

def CreateBMPFile(filename):
    global x, y, wd, ht, multistate, colors, state0

    # create 2D array of pixels filled initially with state 0 color
    pixels = [[state0 for col in xrange(wd)] for row in xrange(ht)]
   
    cellcount = 0
    for row in xrange(ht):
       # get a row of cells at a time to minimize use of Python memory
       cells = g.getcells( [ x, y + row, wd, 1 ] )
       clen = len(cells)
       if clen > 0:
          inc = 2
          if multistate:
             # cells is multi-state list (clen is odd)
             inc = 3
             if clen % 3 > 0: clen -= 1    # ignore last 0
          for i in xrange(0, clen, inc):
             if multistate:
                n = cells[i+2] * 4 + 1
                pixels[row][cells[i]-x] = (colors[n],colors[n+1],colors[n+2])
             else:
                pixels[row][cells[i]-x] = (colors[5],colors[6],colors[7])
             cellcount += 1
             if cellcount % 1000 == 0:
                # allow user to abort huge pattern/selection
                g.dokey( g.getkey() )
   
    WriteBMP(pixels, filename)

# --------------------------------------------------------------------

# remove any existing extension from layer name
outname = g.getname().split('.')[0]

# prompt user for number of generations (= number of files)
numgens = g.getstring("The number of generations will be\n" +
                      "the number of .bmp files created.",
                      "100", "How many generations?")
if len(numgens) == 0:
    g.exit()
if not validint(numgens):
    g.exit('Sorry, but "' + numgens + '" is not a valid integer.')

numcreated = 0
intgens = int(numgens)
while intgens > 0:
   outfile = outname + "_" + g.getgen() + ".bmp"
   g.show("Creating " + outfile)
   CreateBMPFile(outfile)
   numcreated += 1
   g.run(1)
   g.update()
   intgens -= 1

g.show("Number of .bmp files created: " + str(numcreated))
