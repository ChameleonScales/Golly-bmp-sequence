# Golly BMP sequence
These are plug-ins for the cellular automata simulator [Golly](http://golly.sourceforge.net/).
They let you export a sequence of bmp images bounded by the rectangle selection tool.
You only need to install either one of them. They do the same thing, they're just written in different languages.

These scripts were made by [Andrew Trevorrow](https://conwaylife.com/wiki/Andrew_Trevorrow), developer of Golly.
Here is the [forum thread](https://conwaylife.com/forums/viewtopic.php?f=9&t=1333&p=41544&hilit=bmp+sequence#p41544) that started this project.

## Install instructions :
1. Go into your installed (or portable) Golly folder, then under Scripts
2. add a folder named "my-scripts"
3. Paste either of the 2 scripts inside of it

    ⚠ If you use a system installation of Golly, there may be rights restrictions in this folder. You have to to allow writing in it in order to make the image export work
4. Open Golly
5. Prepare a simulation
6. Select a rectangle
7. In the left panel, go to Scripts > my-scripts and click on BMP-sequence
8. A popup should appear asking you the number of frames you want to export
9. Your images will be exported in the same folder as your script